FROM nginx:latest
WORKDIR /usr/share/nginx/html
COPY index.html Monkey.png .
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
